#ifndef KOLO_H
#define KOLO_H

#include <QWidget>
#include <QPainter>
#include <QtGui>


class Kolo : public QWidget
{
    Q_OBJECT

public:
    explicit Kolo(QWidget *parent = 0);
    void paintEvent(QPaintEvent *event);
    void resizeEvent(QResizeEvent * event);
    QSize sizeHint();
    ~Kolo();
signals:
    
public slots:
    void aktualizuj(float relax);

private:
    int lastHeight;
    float skala;
};

#endif // KOLO_H
