#-------------------------------------------------
#
# Project created by QtCreator 2016-05-11T09:22:17
#
#-------------------------------------------------

QT       += core gui
QT       += opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Projekt_EEG
TEMPLATE = app


SOURCES += main.cpp\
    mainwindow.cpp \
    kolo.cpp \
    wczytywacz.cpp \
    wykres.cpp \
    analizerrelaxu.cpp

HEADERS  += mainwindow.h \
    kolo.h \
    wczytywacz.h \
    wykres.h \
    analizerrelaxu.h

FORMS    += mainwindow.ui \

DISTFILES +=
