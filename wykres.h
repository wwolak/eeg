#ifndef WYKRES_H
#define WYKRES_H

#include <vector>

#include <QWidget>
#include <QPainter>
#include <QtGui>

using namespace std;


class Wykres : public QWidget
{
    Q_OBJECT
public:
    explicit Wykres(QWidget *parent = 0);
    void paintEvent(QPaintEvent *event);
    void resizeEvent(QResizeEvent * event);
signals:

public slots:
    void aktualizuj(vector<int> data);
private:
    vector<int> data;
    double y_min;
    double y_max;
};

#endif // WYKRES_H
