#ifndef WCZYTYWACZ_H
#define WCZYTYWACZ_H
#include <string>
#include <fstream>
#include <QObject>

using namespace std;


class Wczytywacz : public QObject
{
    Q_OBJECT

public:
    Wczytywacz(int dl_bufora);
    void wczytaj();

public slots:
    void ustaw_plik(string nazwa_pliku);

signals:
   void wczytano_dane(string bufor); // FIXME bufor będzie wektorem intów
   void zmieniono_plik(string nazwa_pliku);

private:
    string nazwa_pliku;
    string bufor; // FIXME
    int dl_bufora;
    ifstream plik;
};

#endif // WCZYTYWACZ_H
