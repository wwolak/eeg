#include "wykres.h"

Wykres::Wykres(QWidget *parent) :
    QWidget(parent)
{
    this -> y_min = 0.0;
    this -> y_max = 1024.0;
   // Dane testowe:
   //
    vector<int> data;
    data.push_back(0);
    data.push_back(10);
    data.push_back(50);
    data.push_back(1024);
    data.push_back(92);
    data.push_back(80);
    data.push_back(65);
    data.push_back(45);
    data.push_back(20);
    data.push_back(0);
    this -> aktualizuj(data);
}

void Wykres::aktualizuj(vector<int> data)
{
    this -> data = data;
    this -> update();
}

void Wykres::resizeEvent(QResizeEvent * event) {
    QWidget::resizeEvent(event);
    // Może kiedyś coś tu będzie innego
}

void Wykres::paintEvent(QPaintEvent *event)
{

    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing, true);
    painter.setPen(QPen(Qt::black, 4, Qt::SolidLine, Qt::RoundCap));
    painter.setBrush(QBrush(Qt::black, Qt::SolidPattern));



    QRect r = event->rect();
    // Punkty wyróżnione na wykresie
    int margin = 10;
    QPoint zero = r.bottomLeft() + QPoint(margin, -margin);
    QPoint up = r.topLeft() + QPoint(margin, margin);
    QPoint right = r.bottomRight() + QPoint(-margin, -margin);
    // relatywne wartości w postaci punktów
    QPoint x = right - zero;
    QPoint y = up - zero;


    // Rysowanie osi:
    painter.drawLine(QPointF(zero), QPointF(up));
    painter.drawLine(QPointF(zero), QPointF(right));

    if (this -> data .size() < 2) return;

    QPointF dx = QPointF(x) / (this -> data.size() - 1);

    double dy_data = this -> y_max - this-> y_min;

    // Wypełnianie wykresu danymi
    QVector<QPointF> punkty_danych;
    int i=0;
    for(vector<int>::iterator it = ++ (this -> data.begin()) ;
        it != this -> data.end() ; it++)
    {
        punkty_danych.push_back(zero + dx * i + y * (*(it-1) - this -> y_min) / dy_data);
        punkty_danych.push_back(zero + dx * (i+1) + y * (*it - this -> y_min) / dy_data);
        i++;
    }

    // Rysowanie wykresu:
    painter.setPen(QPen(Qt::red, 4, Qt::SolidLine, Qt::RoundCap));
    painter.setBrush(QBrush(Qt::red, Qt::SolidPattern));
    painter.drawLines(punkty_danych);
}
