#ifndef ANALIZERRELAXU_H
#define ANALIZERRELAXU_H

#include <vector>
#include <algorithm>

#include <QObject>

using namespace std;


class AnalizerRelaxu : public QObject
{
    Q_OBJECT
public:
    explicit AnalizerRelaxu(QObject *parent = 0);

signals:
    void relax(double relax);

public slots:
    void analizuj(vector<int> dane);
};

#endif // ANALIZERRELAXU_H
