#include "kolo.h"


Kolo::Kolo(QWidget *parent) :
    QWidget(parent)
{
    this -> sizePolicy() .setWidthForHeight(true);
    this -> aktualizuj(1.0);
}


Kolo::~Kolo()
{
}

QSize Kolo::sizeHint() {
    QSize s = size();
    lastHeight = s.height();
    s.setWidth(s.height());
    return s;
}

void Kolo::resizeEvent(QResizeEvent * event) {
    QWidget::resizeEvent(event);

    if (lastHeight!=height()) {
        updateGeometry();
    }
}

void Kolo::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing, true);
    painter.setPen(QPen(Qt::red,4 , Qt::SolidLine, Qt::RoundCap));
    painter.setBrush(QBrush(Qt::red, Qt::SolidPattern));
    QRect r = event -> rect();
    QPoint c = r.center();
    r.setHeight(r.height() * this -> skala);
    r.setWidth(r.width() * this -> skala);
    r.translate(c - r.center());
    painter.drawEllipse(r);
}

void Kolo::aktualizuj(float relax)
{
    this -> skala = relax;
    this -> update();
}
