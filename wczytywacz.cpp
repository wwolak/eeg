#include "wczytywacz.h"


Wczytywacz::Wczytywacz(int dl_bufora)
{
    this -> dl_bufora = dl_bufora;
    this -> nazwa_pliku = "";
}


void Wczytywacz::ustaw_plik(string nazwa_pliku)
{
    this -> nazwa_pliku = nazwa_pliku;
    this -> plik.open(this -> nazwa_pliku .data());

    emit zmieniono_plik(nazwa_pliku);
}


void Wczytywacz::wczytaj(){
    // Wczytuje po dane linia po linii
    // Gdy wczyta pustą linię przerywa wczytywanie
    // Gdy wczyta wystarczającą ilość danych to emituje dane
}


